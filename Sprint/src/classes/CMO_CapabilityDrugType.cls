public with sharing class CMO_CapabilityDrugType {

    public Id recTypeId{get;set;}
    public Id siteId{get;set;}
    public Id orgId{get;set;}
    public String recType{get;set;}
    public String site{get;set;}
    public String org{get;set;}
    public Id capId{get; set;}
    //CMO_Location__c site{get;set;}
    //Account org{get;set;}
    CMO_Capability__c record;

    public CMO_CapabilityDrugType(ApexPages.StandardController controller) {
        record = (CMO_Capability__c)controller.getRecord();
        
        //Capability Creation
        recTypeId = ApexPages.currentPage().getParameters().get('RecordType');
        siteId = ApexPages.currentPage().getParameters().get('SiteId');
        orgId = ApexPages.currentPage().getParameters().get('orgId');

        //Capability Edit
        if(siteId == NULL){
            capId = record.Id;
            List<CMO_Capability__c> capList = [SELECT Id, RecordTypeId, CMO_Site__c, CMO_Organization__c FROM CMO_Capability__c WHERE Id =: capId];
            siteId = capList[0].CMO_Site__c;
            recTypeId = capList[0].RecordTypeId;
            orgId = capList[0].CMO_Organization__c; 
        }
        
        recType = [SELECT Name FROM RecordType where Id =: recTypeId limit 1].Name;
        site = [SELECT Name FROM CMO_Location__c where Id =: siteId limit 1].Name;
        org = [SELECT Name FROM Account where Id =: orgId limit 1].Name;
        
    }
    
    
    public pageReference SaveCapability(){
        pageReference detailPage = null;
            CMO_Capability__c cap = new CMO_Capability__c();
            //cap.CMO_Capability_Name__c = record.CMO_Capability_Name__c;
            if(capId != NULL){
                cap.Id = capId;
            }
            cap.CMO_Site__c = siteId;
            cap.CMO_Organization__c = orgId;
            cap.RecordTypeId = recTypeId;
            cap.CMO_Other_Roller_Compaction_Manufacture__c = record.CMO_Other_Roller_Compaction_Manufacture__c;
            cap.CMO_Manufacture_Solid_Oral_Dosage__c = record.CMO_Manufacture_Solid_Oral_Dosage__c;
                if(cap.CMO_Manufacture_Solid_Oral_Dosage__c == true){
                    cap.CMO_Hot_melt_extrusion__c = record.CMO_Hot_melt_extrusion__c;
                    cap.CMO_Spray_drying__c = record.CMO_Spray_drying__c;
                    cap.CMO_Development_Lab_Oral__c = record.CMO_Development_Lab_Oral__c;
                    cap.CMO_Blending__c = record.CMO_Blending__c;
                        if(cap.CMO_Blending__c == true){
                            cap.CMO_Blending_Scale_kg_min__c = record.CMO_Blending_Scale_kg_min__c;
                            cap.CMO_Blending_Scale_kg_max__c = record.CMO_Blending_Scale_kg_max__c;
                            cap.CMO_Bin_Blender__c = record.CMO_Bin_Blender__c;
                            cap.CMO_V_Blending__c = record.CMO_V_Blending__c;
                        }
                    cap.CMO_Roller_Compaction__c = record.CMO_Roller_Compaction__c;
                        if(cap.CMO_Roller_Compaction__c == true){
                            cap.CMO_Roller_Compaction_Scale_kg_min__c = record.CMO_Roller_Compaction_Scale_kg_min__c;
                            cap.CMO_Roller_Compaction_Scale_kg_max__c = record.CMO_Roller_Compaction_Scale_kg_max__c;
                            cap.CMO_Roller_Compaction_Manufacture_s__c = record.CMO_Roller_Compaction_Manufacture_s__c;
                            cap.CMO_Other_Roller_Compaction_Manufacture__c = record.CMO_Other_Roller_Compaction_Manufacture__c;
                        }
                    cap.CMO_High_Shear_Wet_Granulation__c = record.CMO_High_Shear_Wet_Granulation__c;
                        if(cap.CMO_High_Shear_Wet_Granulation__c == true){
                            cap.CMO_Granulation_Scale_kg_min__c = record.CMO_Granulation_Scale_kg_min__c;
                            cap.CMO_Granulation_Scale_kg_max__c = record.CMO_Granulation_Scale_kg_max__c;
                        }
                    cap.CMO_Fluid_Bed_Granulation_Drying__c = record.CMO_Fluid_Bed_Granulation_Drying__c;
                        if(cap.CMO_Fluid_Bed_Granulation_Drying__c == true){
                            cap.CMO_FBG_D_Scale_kg_min__c = record.CMO_FBG_D_Scale_kg_min__c;
                            cap.CMO_FBG_D_Scale_kg_max__c = record.CMO_FBG_D_Scale_kg_max__c;
                        }
                    cap.CMO_Tableting__c = record.CMO_Tableting__c;
                        if(cap.CMO_Tableting__c == true){
                            cap.CMO_Tableting_Scale_kg_min__c = record.CMO_Tableting_Scale_kg_min__c;
                            cap.CMO_Tableting_Scale_kg_max__c = record.CMO_Tableting_Scale_kg_max__c;
                            cap.CMO_Tableting_Manufacture_s__c = record.CMO_Tableting_Manufacture_s__c;
                            cap.CMO_Other_Tableting_Manufacture__c = record.CMO_Other_Tableting_Manufacture__c;
                        }
                    cap.CMO_Encapsulation__c = record.CMO_Encapsulation__c;
                        if(cap.CMO_Encapsulation__c == true){
                            cap.CMO_Encapsulation_Scale_kg_min__c = record.CMO_Encapsulation_Scale_kg_min__c;
                            cap.CMO_Encapsulation_Scale_kg_max__c = record.CMO_Encapsulation_Scale_kg_max__c;
                            cap.CMO_Automatic__c = record.CMO_Automatic__c;
                            cap.CMO_Semi_Automatic_Handfill__c = record.CMO_Semi_Automatic_Handfill__c;
                        }
                    cap.CMO_Coating__c = record.CMO_Coating__c;
                        if(cap.CMO_Coating__c == true){
                            cap.CMO_Coating_Scale_kg_min__c = record.CMO_Coating_Scale_kg_min__c;
                            cap.CMO_Coating_Scale_kg_max__c = record.CMO_Coating_Scale_kg_max__c;
                        }
                    cap.CMO_Other_Solid_Oral_check__c = record.CMO_Other_Solid_Oral_check__c; 
                    if(cap.CMO_Other_Solid_Oral_check__c == true){
                        cap.CMO_Other_Solid_Oral_Dosage__c = record.CMO_Other_Solid_Oral_Dosage__C;
                    }            
                }
            cap.CMO_Manufacture_Sterile_Vial_Products__c = record.CMO_Manufacture_Sterile_Vial_Products__c;
                if(cap.CMO_Manufacture_Sterile_Vial_Products__c == true){
                    cap.CMO_Conjugation__c = record.CMO_Conjugation__c;
                    cap.CMO_Filled_Vial_Decontamination__c = record.CMO_Filled_Vial_Decontamination__c;
                    cap.CMO_Nanoparticle_Liposome_Processing__c = record.CMO_Nanoparticle_Liposome_Processing__c;
                    cap.CMO_Device_Capability__c = record.CMO_Device_Capability__c;
                    cap.CMO_Depyrogination__c = record.CMO_Depyrogination__c;
                    cap.CMO_Development_Lab__c = record.CMO_Development_Lab__c;
                    cap.CMO_Vial_Washing_Drying__c = record.CMO_Vial_Washing_Drying__c;
                    cap.CMO_Mixing__c = record.CMO_Mixing__c;
                        if(cap.CMO_Mixing__c == true){
                            cap.CMO_Mixing_Volume_Min_Scale_L__c = record.CMO_Mixing_Volume_Min_Scale_L__c;
                            cap.CMO_Mixing_Volume_Max_Scale_L__c = record.CMO_Mixing_Volume_Max_Scale_L__c;
                        }
                    cap.CMO_Aseptic_Filling__c = record.CMO_Aseptic_Filling__c;
                        if(cap.CMO_Aseptic_Filling__c == true){
                            cap.CMO_Cartridges__c = record.CMO_Cartridges__c;
                            cap.CMO_Syringe_Unit_Size_mL_min__c = record.CMO_Syringe_Unit_Size_mL_min__c;
                            cap.CMO_Syringe_Unit_Size_mL_max__c = record.CMO_Syringe_Unit_Size_mL_max__c;
                            cap.CMO_Syringe_Pump_Heads_min__c = record.CMO_Syringe_Pump_Heads_min__c;
                            cap.CMO_Syringe_Pump_Heads_max__c = record.CMO_Syringe_Pump_Heads_max__c;
                            cap.CMO_Syringe_Units_per_Minute_min__c = record.CMO_Syringe_Units_per_Minute_min__c;
                            cap.CMO_Syringe_Units_per_Minute_max__c = record.CMO_Syringe_Units_per_Minute_max__c;
                            cap.CMO_Vials_Unit_Size_mL_min__c = record.CMO_Vials_Unit_Size_mL_min__c;
                            cap.CMO_Vials_Unit_Size_mL_max__c = record.CMO_Vials_Unit_Size_mL_max__c;
                            cap.CMO_Vials_Pump_Heads_min__c = record.CMO_Vials_Pump_Heads_min__c;
                            cap.CMO_Vials_Pump_Heads_max__c = record.CMO_Vials_Pump_Heads_max__c;
                            cap.CMO_Vials_Units_per_Minute_min__c = record.CMO_Vials_Units_per_Minute_min__c;
                            cap.CMO_Vials_Units_per_Minute_max__c = record.CMO_Vials_Units_per_Minute_max__c;
                        }
                    cap.CMO_Lyophilization__c = record.CMO_Lyophilization__c;
                        if(cap.CMO_Lyophilization__c == true){
                            cap.CMO_Lyophilization_Minimum_Volume_ft3__c = record.CMO_Lyophilization_Minimum_Volume_ft3__c;
                            cap.CMO_Lyophilization_Min_Shelf_Temperature__c = record.CMO_Lyophilization_Min_Shelf_Temperature__c;
                            cap.CMO_Formats__c = record.CMO_Formats__c;
                        }
                    cap.CMO_Terminal_Sterilization__c = record.CMO_Terminal_Sterilization__c;
                }
                if(capId == NULL){
                    insert cap;
                }else{
                    update cap;
                }
            if(cap!=null){
                //detailPage = new ApexPages.StandardController(cap).view(); 
                pageReference pageRef = new pageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+siteId);
                //pageref.getParameters().put('id',siteId);
                pageRef.setRedirect(true);
                detailPage = pageRef;
            }
        return detailPage;
    }
    
    
    public pageReference cancelCapability(){
        
        //if(ApexPages.currentPage().getParameters().get('SiteId') != NULL){
            PageReference pageRef = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+siteId);
            pageRef.setRedirect(true);
            return pageRef;
        /*}else{
            
            pageReference pageRef = new pageReference('https://takeda-rd--cmodev.cs77.my.salesforce.com/'+capId);
            //pager.getParameters().put('id',capId);
            pageRef.setRedirect(true);
            
            return pageRef;
        }*/
    }
    
    public pageReference editCapability(){
            PageReference pageRef = Page.CMO_Basic_Capability_New_Testfinal;
            //List<CMO_Location__c> siteList = [SELECT CMO_Vendor__c, Id, Name FROM CMO_Location__c where Id = :siteId];
            //input = ApexPages.currentPage().getParameters().get('var');
            pageRef.getParameters().put('id', capId);
            /*pageRef.getParameters().put('OrgId',orgId);
            pageRef.getParameters().put('RecordType',recTypeId);*/
            pageRef.SetRedirect(true); 
            return pageRef;
    }

}